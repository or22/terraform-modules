# Terraform Modules

Within this repo you will find some of the common modules that I use throughout my applications

## Module Structure

These modules should follow the [Terraform recommended structure](https://www.terraform.io/docs/modules/create.html)

Each subfolder will have at least three files: `main.tf`, `variables.tf`, and `outputs.tf`

```
$ tree minimal-module/
.
|-- main.tf
|-- variables.tf
|-- outputs.tf
```

### RDS-Aurora-Serverless

This module creates a new serverless database cluster with RDS Aurora, a new Parameter store paramter with the database 
credentials, and a pair of client/server Security Groups for controllin access.

Variables:
* `allow_client_outbound_traffic` (Optional) - Whether to allow other outbound traffic for the client security group (Defaults to false)
* `auto_pause` (Optional) - Whether to enable automatic pause of the database when idle (Defaults to true)
* `data_api` (Optional) - Whether to enable HTTP endpoint for Aurora Serverless Data API (Defaults to false)
* `database_name` - Name for the database
* `engine` (Optional) - Database engine to use (Defaults to `aurora-postgresql`, also supports `aurora-mysql`)
* `env_prefix` - Prefix used to namespace resources
* `master_db_user` - Master username for the database
* `master_db_password` - Master password for the database
* `max_capacity` (Optional) - Maximum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)
* `min_capacity` (Optional) - Minimum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)
* `seconds_until_auto_pause` (Optional) - The time, in seconds, before the database will be paused when idle (Defaults to 3600)
* `security_group_ids` (Optional) - List of additional VPC Security Group Ids needing access to the database
* `subnet_ids` - List of VPC Subnet Ids to use for database network interfaces
* `kms_key_id` (Optional) - The kms key identifier (alias is ok too) used for encrypting the database config SSM parameter (Defaults to null which is aws kms)

Outputs:

* `rds_client_security_group_id` - Security Group Id for clients to access RDS Aurora Cluster
* `rds_cluster_arn` - ARN for the Aurora Cluster
* `rds_cluster_database_name` - Name of the database
* `rds_cluster_endpoint` - Endpoint to use when connecting to the Aurora Cluster
* `rds_cluster_maintenance_window` - Maintenance window for the Aurora Cluster
* `rds_cluster_master_username` - Username to use when connecting to the Aurora Cluster
* `rds_cluster_port` - Port to use when connecting to the Aurora Cluster
* `parameter_group_name` - The parameter name that holds the Aurora Cluster configuration
* `parameter_group_kms_key_id` - The KMS key id used for encrypting the Aurora Cluster configuration

**IMPORTANT:** RDS has certain naming constraints for some of these variables: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Limits.html#RDS_Limits.Constraints

**NOTE:** Changes to a running database are not applied immediately, and instead are applied during the next available maintenance window: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.DBInstance.Modifying.html

**NOTE:** Termination protection is enabled on the Aurora cluster with this module. If the cluster needs to be deleted you will have to disable the protection through the AWS console first.

**NOTE:** Aurora Serverless limitations: https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/aurora-serverless.html#aurora-serverless.limitations

**NOTE:** As of 6/1/2020 Aurora Serverless now supports a min_capacity of 1 for MySQL, but still requires a min_capacity of 2 for Postgres
